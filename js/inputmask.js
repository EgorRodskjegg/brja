$(document).ready(function(){	
	
	$('.tel').inputmask({ mask: "+7 (999) 999-99-99", clearIncomplete: true,
		onincomplete: function(){
            $(this).parent('.form-block__row').addClass('error');               				
        },
		oncomplete: function(){
             $(this).parent('.form-block__row').removeClass('error');                				
        }			
	});		

	$('.email').inputmask({
        mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]", 
        greedy: false,
        onBeforePaste: function (pastedValue, opts) {
            pastedValue = pastedValue.toLowerCase();
            return pastedValue.replace("mailto:", "");
        },
        definitions: {
            '*': {
                validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                cardinality: 1,
                casing: "lower"
            }
        },
		onincomplete: function(){
            $(this).parent('.form-block__row').addClass('error'); 				
        },
		oncomplete: function(){
             $(this).parent('.form-block__row').removeClass('error');				
        }				
	});		
	
	$('.req-field').focusout(function(){
		if($(this).val() != ''){          
			$(this).parent('.form-block__row').removeClass('error');			
		}else{
			$(this).parent('.form-block__row').addClass('error');		
		} 
	}); 
		
});



	
	
	
	
	
	
	
	