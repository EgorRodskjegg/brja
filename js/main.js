$('document').ready(function() {
//menu
	$('#menu-btn').click(function(){		
		$('.drop-menu').slideToggle(1000);	
		if ($(this).hasClass('not-active')) {
			$(this).addClass('is-active').removeClass('not-active');
		}else{
			setTimeout(function(){
				$('.is-active').addClass('not-active').removeClass('is-active')
			},900)
		};		
	});

	$('.avatar-block').click(function(){
		$(this).next('.header__user-menu').slideToggle(400);
		$(this).toggleClass('active');
	});	

//registration-buttons	
	$('.reg').click(function(){		
		$(this).parent('.btn-reg').children('.reg-translator').addClass('is-visible');
		$(this).parent('.btn-reg').children('.reg-client').addClass('is-visible');
		$(this).fadeOut(400);
	});

//link form-block__open-descr
	$('.form-block__open-descr').click(function(){
		$(this).parent('.form-block__row').next('.form-block__descr').slideDown(700);
		$(this).css('display','none');
		return false

	});

	$('.plus-minus button').click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('minus-btn')) {
			$(this).siblings('input').val(function(i,val) {
				return +val-1
			});
		} else {
			$(this).siblings('input').val(function(i,val) {
			return +val+1
			});
		}
	});
		
});