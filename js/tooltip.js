var app = {};

(function($) {
	$.fn.makeTooltips = (function(tipArrow, tipCloseBtn, tipPosition, tipWidth, tipStyle, tipStartEvent) {
		$(this).each(function() {
		var self = this;
		var Tooltip = function(hasArrow, hasCloseBtn, position, width, style){
			this.parent = $(self);
			this.arrow = hasArrow;
			this.closeBtn = hasCloseBtn;
			this.position = position;
			this.startEvent = tipStartEvent;
			this.size = width;
			this.style = style;
		};

		var tooltip = new Tooltip(tipArrow, tipCloseBtn, tipPosition, tipWidth, tipStyle, tipStartEvent);
		var buildFrame = (function() {
			var addNewClass = tooltip.position + " ";
			var end = "";
			if(tooltip.arrow){
				addNewClass += "tooltip__arrow ";
			}

			if(tooltip.closeBtn){
				end = "<span class='tooltip__close'><span></div></div>";
				addNewClass += "tooltip--closeBtn ";
			}else{
				end = "</div></div>"
			}

			if(tooltip.size == 'medium'){
				addNewClass += "tooltip--medium ";
			}else if(tooltip.size == 'large'){
				addNewClass += "tooltip--large ";
			}else if(tooltip.size == 'small'){
				addNewClass += "tooltip--small ";
			}

			if(tooltip.style == 'lighter'){
				addNewClass += "tooltip--lighter ";
			}else if(tooltip.style == 'light'){
				addNewClass += "tooltip--light ";
			}
			var start ="<div class='tooltip " + addNewClass + "'><div class='tooltip__in'>";
			
			tooltip.parent.each(function(){
				text = $(this).attr('data-tooltip');
			});
			var frame = start + text + end;
			return frame;
		});

		var tooltipFrame = buildFrame();

		tooltip.parent.each(function(){
			var parent = $(this);
			parent.after(tooltipFrame);	
			var currentTip = $(parent.next(".tooltip"));
			
			var base = {
				"X": parent.position().left,
				"Y": parent.position().top,
				"H": parent.height(),
				"W": parent.width()
			};
			var tip = {
				"offsetY": 10, //how far the tooltip positions from parent
				"offsetX": 10,
				"H": currentTip.height(),
				"W": currentTip.width(),
				"Y": currentTip.position().left,
				"X": currentTip.position().top,
				"marLeft": 0,
				"marTop": 0
			};

			var placement = tooltip.position;
			switch(placement)
			{
				case "top":
					tip.Y = base.Y - tip.H -tip.offsetY;
					tip.X = base.X + base.W/2;
					tip.marLeft = -tip.W/2;
					break;
				case "bottom":
					tip.Y = base.Y + base.H + tip.offsetY;
					tip.X = base.X + base.W/2;
					tip.marLeft = -tip.W/2;
					break;
				case "left":
					tip.X = base.X - tip.W - tip.offsetX;
					tip.Y = base.Y + base.H/2;
					tip.marTop =  - tip.H/2;
					break;
				case "right":
					tip.X = base.X + base.W + tip.offsetX;
					tip.Y = base.Y + base.H/2;
					tip.marTop = -tip.H/2;
					break;
				case "topLeft":
					tip.Y = base.Y - tip.H -tip.offsetY;
					tip.X = base.X + base.W/2;
					tip.marLeft = -tip.W/2-25;
					break;
				case "topRight":
					tip.Y = base.Y - tip.H -tip.offsetY;
					tip.X = base.X + base.W/2;
					tip.marLeft = -tip.W/2+25;
					break;
				case "bottomRight":
					tip.Y = base.Y + base.H + tip.offsetY;
					tip.X = base.X + base.W/2;
					tip.marLeft = -tip.W/2+25;
					break;
				case "bottomLeft":
					tip.Y = base.Y + base.H + tip.offsetY;
					tip.X = base.X + base.W/2;
					tip.marLeft = -tip.W/2-25;
					break;
			}
			currentTip.css({top: tip.Y, left: tip.X, marginLeft: tip.marLeft, marginTop: tip.marTop});
		});
		
		var showHideTooltips = (function() {
			if(tooltip.startEvent == 'hover'){
				tooltip.parent.each(function() {
					var parent = $(this);
					var currentTip = $(parent.next(".tooltip--light, .tooltip--lighter"));
					parent.on('mouseenter', function() {
						currentTip.show();
					});
					$(currentTip).on('click', function(e) {
					    e.stopPropagation();

					});
					$(window).on('click', function () {
						$('.tooltip--light, .tooltip--lighter').hide();
					});
				});
			} else if(tooltip.startEvent == 'onload'){
				tooltip.parent.each(function() {
					var parent = $(this);
					var currentTip = $(parent.next(".tooltip--closeBtn"));
					currentTip.show();
				});
				var currentBtn = $(".tooltip__close");
				currentBtn.on('click', function() {
					var btnParent = $(this).parent().parent();
					btnParent.hide();
				});
			}
		})();
			
		});

		
	});	

	//Big Tooltips, dark grey background, white text
	$('.has-tooltip-darkTop').makeTooltips(true, false, 'top', 'large', 'dark');
	$('.has-tooltip-darkBottom').makeTooltips(true, false, 'bottom', 'large', 'dark');
	$('.has-tooltip-darkBottomRight').makeTooltips(true, false, 'bottomRight', 'large', 'dark');
	$('.has-tooltip-darkBottomLeft').makeTooltips(true, false, 'bottomLeft', 'large', 'dark');
	$('.has-tooltip-darkLeft').makeTooltips(true, false, 'left', 'large', 'dark');
	$('.has-tooltip-darkRight').makeTooltips(true, false, 'right', 'large', 'dark');
	$('.has-tooltip-darkTopRight').makeTooltips(true, false, 'topRight', 'large', 'dark');
	$('.has-tooltip-darkTopLeft').makeTooltips(true, false, 'topLeft', 'large', 'dark');

	//Big tooltips with arrows
	// $('.has-tooltip-darkTop-btn').each(function() {$(this).makeTooltips(true, true, 'top', 'large', 'dark', 'onload');});
	$('.has-tooltip-darkTop-btn').makeTooltips(true, true, 'top', 'large', 'dark', 'onload');
	$('.has-tooltip-darkBottom-btn').makeTooltips(true, true, 'bottom', 'large', 'dark', 'onload');
	$('.has-tooltip-darkBottomRight-btn').makeTooltips(true, true, 'bottomRight', 'large', 'dark', 'onload');
	$('.has-tooltip-darkBottomLeft-btn').makeTooltips(true, true, 'bottomLeft', 'large', 'dark', 'onload');
	$('.has-tooltip-darkLeft-btn').makeTooltips(true, true, 'left', 'large', 'dark', 'onload');
	$('.has-tooltip-darkRight-btn').makeTooltips(true, true, 'right', 'large', 'dark', 'onload');
	$('.has-tooltip-darkTopRight-btn').makeTooltips(true, true, 'topRight', 'large', 'dark', 'onload');
	$('.has-tooltip-darkTopLeft-btn').makeTooltips(true, true, 'topLeft', 'large', 'dark', 'onload');

	//Small tooltip, blue border, blue text
	$('.has-tooltip-lightTop').makeTooltips(true, false, 'top', 'small', 'light', 'hover');
	$('.has-tooltip-lightLeft').makeTooltips(true, false, 'left', 'small', 'light', 'hover');
	$('.has-tooltip-lightRight').makeTooltips(true, false, 'right', 'small', 'light', 'hover');
	$('.has-tooltip-lightBottom').makeTooltips(true, false, 'bottom', 'small', 'light', 'hover');

	//Small tooltip, blue border, gray text
	$('.has-tooltip-lighterTop').makeTooltips(true, false, 'top', 'small', 'lighter', 'hover');
	$('.has-tooltip-lighterLeft').makeTooltips(true, false, 'left', 'small', 'lighter', 'hover');
	$('.has-tooltip-lighterRight').makeTooltips(true, false, 'right', 'small', 'lighter', 'hover');
	$('.has-tooltip-lighterBottom').makeTooltips(true, false, 'bottom', 'small', 'lighter', 'hover');
})(jQuery);